#include "definitions.h"

void floyd_warshall(int graph[][VERTICES]) {
  int i, j, k;
  int dist[VERTICES][VERTICES];
	int near[VERTICES][VERTICES];

	for (i = 0; i < VERTICES; i++) {
	  for(j = 0; j < VERTICES; j++) {
	    dist[i][j]=graph[i][j];
	    if(i == j)
	      near[i][j] = INFINITE;
      if(graph[i][j] != INFINITE)
        near[i][j] = i;
      else
        near[i][j] = INFINITE;
    }
  }

	for(k=0;k<VERTICES;k++)	{
		for(i=0;i<VERTICES;i++)	{
			for(j=0;j<VERTICES;j++)	{
				if(dist[i][k] + dist[k][j] < dist[i][j]) {
				  dist[i][j]=dist[i][k]+dist[k][j];
				  near[i][j]=near[k][j];
			  }
			}
		}
	}

	print_solution(dist);

	for(i=0;i<VERTICES;i++) {
	  for(j=0;j<VERTICES;j++) {
      printf("Caminho entre %d e %d ==> ", i, j);
      if(near[i][j]!=INFINITE) {
        printf("%d<-",j);
	      print_path(near,i,j);
	      printf("\n");
      }
      else {
        printf("Não existe caminho\n");
      }
    }
  }
}

void print_path(int near[][VERTICES], int i, int j) {
  if (near[i][j] == i) {
	  printf("%d",i);
	  return;
  }
  if (near[i][j] != INFINITE) {
    printf("%d<-", near[i][j]);
    print_path(near, i, near[i][j]);
  }
}

void print_solution(int dist[][VERTICES]) {
  printf("A matriz a seguir mostra a distância mais curta "
         "entre cada par de vértice\n\n");

  for (int i = 0; i < VERTICES; i++) {
    for (int j = 0; j < VERTICES; j++) {
      if (dist[i][j] == INFINITE) {
        printf("%7s", "INF");
      }
      else {
        printf("%7d", dist[i][j]);
      }
    }
    printf("\n");
  }
  printf("\n");
}
