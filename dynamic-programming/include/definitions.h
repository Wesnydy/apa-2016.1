#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_

#include <limits.h>
#include <stdio.h>

#define VERTICES 4
#define INFINITE 99999

void floyd_warshall(int graph[][VERTICES]);
void print_path(int graph[][VERTICES], int i, int j);
void print_solution(int dist[][VERTICES]);

#endif // _DEFINITIONS_H_
