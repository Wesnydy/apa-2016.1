#include "definitions.h"

int main(void) {
 // INPUT
 //       10
 //  (0)------->(3)
 //   |          ^
 // 5 |          |
 //   |          | 1
 //   v          |
 //  (1)------->(2)
 //       3

  int graph[VERTICES][VERTICES] = { {0, 5, INFINITE, 10},
                                    {INFINITE, 0, 3, INFINITE},
                                    {INFINITE, INFINITE, 0, 1},
                                    {INFINITE, INFINITE, INFINITE, 0}
                                  };
  floyd_warshall(graph);
  return 0;
}
