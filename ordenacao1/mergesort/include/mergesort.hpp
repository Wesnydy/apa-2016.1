#ifndef MERGESORT_H_
#define MERGESORT_H_

#include <stdlib.h>
#include <vector>

using namespace std;

class MergeSort {
 public:
   MergeSort() {};
   ~MergeSort() {};

   void sort_vector(vector<int> &vec);

 private:
   void mergesort(int *vec, const int size);
   void merge(int *vec, const int size);

   int *vector_temp;
};

#endif // MERGESORT_H_
