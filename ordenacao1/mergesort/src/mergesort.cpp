#include "mergesort.hpp"

void MergeSort::sort_vector(vector<int> &vec) {
  int vsize = vec.size();
  int *arr = &vec[0];

  mergesort(arr, vsize);
}

void MergeSort::mergesort(int *vec, const int size) {
  if (size > 1) {
    int middle = size / 2;
    mergesort(vec, middle);
    mergesort(vec + middle, size - middle);
    merge(vec, size);
  }
}

void MergeSort::merge(int *vec, const int size) {
  vector_temp = new (nothrow) int [size];
  if (vector_temp == nullptr) {
    exit(1);
  }

  int middle = size / 2;
  int i = 0;
  int j = middle;
  int k = 0;

  while (i < middle && j < size) {
    if (vec[i] <= vec[j]) {
      vector_temp[k] = vec[i];
      i++;
    }
    else {
      vector_temp[k] = vec[j];
      j++;
    }
    k++;
  }

  if (i == middle) {
    while (j < size) {
      vector_temp[k] = vec[j];
      k++;
      j++;
    }
  }
  else {
    while (i < middle) {
      vector_temp[k] = vec[i];
      k++;
      i++;
    }
  }

  for (int index = 0; index < size; index++) {
    vec[index] = vector_temp[index];
  }

  delete[] vector_temp;
}
