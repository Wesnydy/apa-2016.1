#ifndef SELECTIONSORT_H_
#define SELECTIONSORT_H_

#include <vector>

using namespace std;

class SelectionSort {
 public:
   SelectionSort() {};
   ~SelectionSort() {};

   void sort_vector(vector<int> &vec);
};

#endif // SELECTIONSORT_H_
