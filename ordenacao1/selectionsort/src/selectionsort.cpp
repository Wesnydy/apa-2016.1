#include "selectionsort.hpp"

void SelectionSort::sort_vector(vector<int> &vec) {
  int temp;
  int vsize = vec.size();

  int i, j, k;

  for (i = 0; i < vsize; i++) {
    for (j = i, k = i; j < vsize; j++) {
      if (vec[j] < vec[k]) {
        k = j;
      }
    }
    temp = vec[i];
    vec[i] = vec[k];
    vec[k] = temp;
  }
}
