#ifndef HEAPSORT_H_
#define HEAPSORT_H_

#include <vector>

using namespace std;

class HeapSort {
 public:
   HeapSort() {};
   ~HeapSort() {};

   void sort_vector(vector<int> &vec);

 private:
   void maxheapify(vector<int> &heap, int i, int max);
   void buildmaxheap(vector<int> &vec, const int size);
   void heapsort(vector<int> &vec, const int size);
};

#endif // HEAPSORT_H_
