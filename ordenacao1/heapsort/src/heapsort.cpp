#include "heapsort.hpp"

void HeapSort::sort_vector(vector<int> &vec) {
  int vsize = vec.size();
  heapsort(vec, vsize);
}

void HeapSort::heapsort(vector<int> &vec, const int size) {
  buildmaxheap(vec, size);
  int end = size - 1;
  while (end > 0) {
    swap(vec[0], vec[end]);
    maxheapify(vec, 0, end);
    end--;
  }
}

void HeapSort::buildmaxheap(vector<int> &vec, const int size) {
  int i = (size/2) - 1;
  while(i >= 0) {
    maxheapify(vec, i, size);
    i--;
  }
}

void HeapSort::maxheapify(vector<int> &heap, int i, int max) {
  int largest;
  int left;
  int right;

  while(i < max) {
    largest = i;
    left = (2*i) + 1;
    right = left + 1;

    if( left < max && heap[left] > heap[largest] )
      largest = left;

    if( right < max && heap[right] > heap[largest] )
      largest = right;

    if(largest == i)
      return;

    swap(heap[i], heap[largest]);
    i = largest;
  }
}
