#include "insertionsort.hpp"

void InsertionSort::sort_vector(vector<int> &vec) {
  int temp;
  int vsize = vec.size();

  for(int i = 1; i < vsize; ++i) {
		temp = vec[i];
		int j = i;
		while(j > 0 && temp < vec[j - 1]) {
			vec[j] = vec[j - 1];
			j--;
		}
		vec[j] = temp;
	}
}
