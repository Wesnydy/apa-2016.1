#include "bubblesort.hpp"

void Bubblesort::sort_vector(vector<int> &vec) {
  int temp;
  bool swap = true;
  int vsize = vec.size();

  for (int i = 0; i < vsize && swap; i++) {
    swap = false;
    for (int j = 0; j < vsize-1; j++) {
      if(vec[j] > vec[j+1]) {
        temp = vec[j+1];
        vec[j+1] = vec[j];
        vec[j] = temp;
        swap = true;
      }
    }
  }
}
