#include <iostream>
#include <vector>

void sort_vectors(std::vector<char> &vecrgb, std::vector<int> &vecvalues) {
  char ctemp;
  int itemp; // Variavel auxiliar
  bool swap = true;
  int vsize = vecvalues.size(); // Quantidade de elementos no vetor

  for (int i = 0; i < vsize && swap; i++) {
    swap = false;
    for (int j = 0; j < vsize-1; j++) {
      if(vecrgb[j] < vecrgb[j+1]) {
        ctemp = vecrgb[j+1];
        vecrgb[j+1] = vecrgb[j];
        vecrgb[j] = ctemp;

        itemp = vecvalues[j+1];
        vecvalues[j+1] = vecvalues[j];
        vecvalues[j] = itemp;
        swap = true;
      }
    }
  }
}

int main() {
  std::vector<int> *ivec = new std::vector<int>();
  ivec->push_back(7);
  ivec->push_back(2);
  ivec->push_back(9);
  ivec->push_back(6);
  ivec->push_back(3);
  ivec->push_back(0);
  ivec->push_back(4);
  ivec->push_back(8);
  ivec->push_back(5);
  ivec->push_back(1);

  std::vector<char> *cvec = new std::vector<char>();
  cvec->push_back('B');
  cvec->push_back('B');
  cvec->push_back('R');
  cvec->push_back('G');
  cvec->push_back('R');
  cvec->push_back('R');
  cvec->push_back('G');
  cvec->push_back('B');
  cvec->push_back('R');
  cvec->push_back('G');

  std::clog << "Input vector1: [ ";
  for (std::vector<int>::const_iterator i = ivec->begin(); i != ivec->end(); i++) {
    std::clog << *i << " ";
  }
  std::clog << "]\n";

  std::clog << "Input vector2: [ ";
  for (std::vector<char>::const_iterator i = cvec->begin(); i != cvec->end(); i++) {
    std::clog << *i << " ";
  }
  std::clog << "]\n\n";

  sort_vectors(*cvec, *ivec);

  std::clog << "Output vector1: [ ";
  for (std::vector<int>::const_iterator i = ivec->begin(); i != ivec->end(); i++) {
    std::clog << *i << " ";
  }
  std::clog << "]\n";

  std::clog << "Output vector2: [ ";
  for (std::vector<char>::const_iterator i = cvec->begin(); i != cvec->end(); i++) {
    std::clog << *i << " ";
  }
  std::clog << "]\n\n";

}
