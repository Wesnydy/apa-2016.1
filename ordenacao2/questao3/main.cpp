#include <iostream>

/* Busca o indice da primeira occorência de um elemento em um vetor ordenado
 em ordem crescente */
int find_position(const int key, const int *vector, const int vsize) {
  int begin = 0;
  int end = vsize - 1;
  int middle;

  while (begin <= end) {
    middle = (begin + end) / 2;
    while (vector[middle] == key) {
      if (vector[middle-1] != key)
        return middle;

      middle--;
    }
    vector[middle] > key ? end = middle - 1 : begin = middle + 1;
  }
  return -1;
}

int main() {
  int size = 10;
  int arr[10] = {2, 2, 4, 5, 6, 7, 8, 8, 8, 9};

  int key = 8;

  std::clog << "Input: ";
  for (int i = 0; i < size; i++)
    std::clog << arr[i] << " ";
  std::clog << "\n";

  int pos = find_position(key, arr, size);

  std::clog << "Primeira posição da chave " << key << " : " << pos << std::endl;

  return 0;
}
