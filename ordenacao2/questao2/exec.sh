#!/bin/sh

echo "\nRunning tests, please wait ... \n"

BIN=program

for i in test/*."in"; do
  ./$BIN -F $i;
  echo "\n"
done

echo "Tests finalized! \n"
