#ifndef LIST_QUICKSORT_H_
#define LIST_QUICKSORT_H_

#include <cstddef>
#include <iostream>

struct Node {
  int content_;
  Node *next_;
};

void insert(Node **head, int content) {
  Node *node = new Node();
  node->content_ = content;
  node->next_ = (*head);
  (*head) = node;
}

Node * get_tail(Node *ptr) {
  while (ptr != NULL && ptr->next_ != NULL)
    ptr = ptr->next_;
  return ptr;
}

void print(Node *node) {
  while (node != NULL) {
    std::clog << node->content_;
    node = node->next_;
  }
}

Node* partition(Node *head, Node *end, Node **newhead, Node **newend) {
  Node *pivo = end;
  Node *prev = NULL;
  Node *curr = head;
  Node *tail = pivo;

  while (curr != pivo) {
    if (curr->content_ < pivo->content_) {
      if ((*newhead) == NULL)
        (*newhead) = curr;

      prev = curr;
      curr = curr->next_;
    }
    else {
      if (prev)
        prev->next_ = curr->next_;

      Node *temp = curr->next_;
      curr->next_ = NULL;
      tail->next_ = curr;
      tail = curr;
      curr = temp;
    }
  }

  if ((*newhead) == NULL)
    (*newhead) = pivo;

  (*newend) = tail;

  return pivo;
}

Node* quicksort(Node *head, Node *end) {
  if (!head || head == end)
    return head;

  Node *new_head = NULL;
  Node *new_end = NULL;

  Node *pivo = partition(head, end, &new_head, &new_end);

  if (new_head != pivo) {
    Node *temp = new_head;

    while (temp->next_ != pivo)
      temp = temp->next_;

    temp->next_ = NULL;

    new_head = quicksort(new_head, temp);

    temp = ::get_tail(new_head);
    temp->next_ = pivo;
  }

  return new_head;
}

void sort_list(Node **head) {
  (*head) = quicksort(*head, get_tail(*head));
}

#endif // LIST_QUICKSORT_H_
