#include <stdlib.h>
#include <time.h>
#include <getopt.h>

#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>

#include "listquicksort.h"

#define _OUT_EXT_ ".out"

void read_args(char **argv, int argc, std::string &file) {
  int opt;
  int long_index = 0;
  static struct option long_options[] = {
    {"file", required_argument, NULL, 'F'},
    {0, 0, 0, 0}
  };

  while ((opt = getopt_long_only(argc, argv, "F:", long_options, &long_index))!= -1) {
    switch (opt) {
      case 'F':
        file = optarg;
        break;

      case '?':
        // O getopt já mostra o erro no terminal
        break;
    }
  }
}

void write_log(const std::string &algorithm, const std::string &file, Node* node) {
  std::string outfile = file;
  outfile.append(".").append(algorithm)
  .append(_OUT_EXT_);

  std::ofstream out;
  out.open(outfile, std::ios::app);
  while (node != NULL) {
    out << node->content_ << "\n";
    node = node->next_;
  }
}

int main(int argc, char **argv) {
  std::string file = "";
  read_args(argv, argc, file);

  if (file == "") {
    std::clog << "File not found" << std::endl;
    return 1;
  }

  std::ifstream ifs_;
  ifs_.open(file, std::ifstream::in);

  if ( ! (ifs_.is_open() && ifs_.good())) {
    std::clog << "Can't open file" << std::endl;
    return 1;
  }

  Node *list = NULL;

  char *ptr;
  int element;
  std::string current_line;
  while (ifs_.good()) {
    getline(ifs_, current_line);
    element = strtol(current_line.c_str(), &ptr, 10);
    insert(&list, element);
  }

  std::clog << "Running Quick Sort with list on input " << file << std::endl;

  clock_t tStart = clock();
  sort_list(&list);
  double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

  printf("Time elapsed: %.4fsec\n\n", etime);
  write_log("quicksort", file, list);

  return 0;
}
